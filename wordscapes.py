import argparse
import itertools

from nltk.corpus import wordnet

parser = argparse.ArgumentParser(description='Cheating at Wordscapes.')
parser.add_argument('-l', '--letters', help='the letters you can use to make words (no spaces)')
parser.add_argument('--min', type=int, default=3, help='the minimum word length you want to search for')
parser.add_argument('--max', type=int, default=6, help='the maximum word length you want to search for')

args = parser.parse_args()
letters = list(args.letters)
min_len = args.min
max_len = args.max

for size in range(min_len, max_len + 1):    
    words = set()
    for word in itertools.permutations(letters, size):        
        if wordnet.synsets(''.join(word)):
            words.add(''.join(word))
    print(size, *words, sep='\n')
